# Corona Data Analysis

A Jupyter notebook for analysis of corona cases

Read the [wiki-page!](https://gitlab.com/knjakob-blomquist/corona-data-analysis/-/wikis/Corona-Data-Analysis)

This notebook downloads csv-file with daily updates of time series of confirmed 
cases for all provinces and countries from the Humanitarian Data Exchange project. 
The following is stated on their [page on the Novel Coronavirus (COVID-19) Cases
Data](https://data.humdata.org/dataset/novel-coronavirus-2019-ncov-cases):

"Novel Corona Virus (COVID-19) epidemiological data since 22 January 2020.
The data is compiled by the Johns Hopkins University Center for Systems Science
and Engineering (JHU CCSE) from various sources including the World Health
Organization (WHO), DXY.cn. Pneumonia. 2020, BNO News, National Health 
Commission of the People’s Republic of China (NHC), China CDC (CCDC), Hong Kong 
Department of Health, Macau Government, Taiwan CDC, US CDC, Government of Canada,
Australia Government Department of Health, European Centre for Disease 
Prevention and Control (ECDC), Ministry of Health Singapore (MOH). JSU CCSE 
maintains the data on the 2019 Novel Coronavirus COVID-19 (2019-nCoV) Data 
Repository on github. Fields available in the data include Province/State,
Country/Region, Last Update, Confirmed, Suspected, Recovered, Deaths."

This notebook downloads the csv-file containing the confirmed cases 
in a wide-format (new column for each day) for each province, and/or
region and/or country. The link to the file is here:
[time_series_2019-ncov-Confirmed.csv](https://data.humdata.org/hxlproxy/api/data-preview.csv?url=https%3A%2F%2Fraw.githubusercontent.com%2FCSSEGISandData%2FCOVID-19%2Fmaster%2Fcsse_covid_19_data%2Fcsse_covid_19_time_series%2Ftime_series_19-covid-Confirmed.csv&filename=time_series_2019-ncov-Confirmed.csv)

In this format the DataFrame looks like this:
<img src="corona_database_wide_format.png" alt="wide format" width=400 >

After data clean-up the DataFrame looks like this:
<img src="corona_database_cleaned.png" alt="cleaded data" width=400 >

The data is fitted with a Logistic function, 
<img src="CodeCogsEqn.gif" alt="logistic function" >

and will create fitted curves like the follwing:
<img src="China_fitted.png" alt="fitted data" width=400 >

You need Python3.6 or newer, pandas, lmfit, and seaborn
